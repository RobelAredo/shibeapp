package com.example.shibeapp.viewmodel

import android.graphics.Color
import androidx.lifecycle.*
import com.example.shibeapp.model.ShibeRepo
import com.example.shibeapp.view.RvLayoutManager
import com.example.shibeapp.view.ShibeState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShibeViewModel @Inject constructor(private val repo: ShibeRepo) : ViewModel() {
//    lateinit var context: Context
//    private val repo = ShibeRepo(context)

    private var _state = MutableLiveData(ShibeState(true))
    val state: LiveData<ShibeState> get() = _state

    init {
        getShibes()
    }

    private fun getShibes() {
        viewModelScope.launch {
            val all = repo.getShibes()
            _state.value = _state.value?.run { copy(shibes = all) } ?: ShibeState(shibes = all)
        }
    }

    private fun getFavorites() {
        viewModelScope.launch {
            val faves = repo.getFavorites()
            _state.value = _state.value?.run { copy(shibes = faves) } ?: ShibeState(shibes = faves)
        }
    }

    fun updateState(
        toggleFave: Boolean = false,
        layoutManagerPosition: Int? = null
    ) {
        val state = state.value!!
            .run {
                if (toggleFave) {
                    val isFavorite = !isFavorite
                    copy(
                        isFavorite = isFavorite,
                        faveColor = if (isFavorite) Color.RED else Color.WHITE
                    )
                } else this
            }
            .run {
                layoutManagerPosition?.let {
                    copy(
                        layoutManager = when (layoutManagerPosition) {
                            0 -> RvLayoutManager.GRID
                            1 -> RvLayoutManager.STAGGERED
                            else -> RvLayoutManager.LINEAR
                        }
                    )
                } ?: this
            }

        _state.value = state
        if (state.isFavorite) getFavorites() else getShibes()
    }

    class ShibeViewModelFactory(
        private val repo: ShibeRepo
    ) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ShibeViewModel(repo) as T
        }
    }

}