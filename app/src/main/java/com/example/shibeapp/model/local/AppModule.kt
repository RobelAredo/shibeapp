package com.example.shibeapp.model.local

import com.example.shibeapp.model.remote.ShibeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val BASE_URL = "https://shibe.online"
    @Provides
    fun provideInstance(): ShibeService {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().let{ it.create(ShibeService::class.java)}
    }

}