package com.example.shibeapp.model

import android.content.Context
import android.util.Log
import com.example.shibeapp.model.local.ShibeDatabase
import com.example.shibeapp.model.local.entity.Favorite
import com.example.shibeapp.model.local.entity.Shibe
import com.example.shibeapp.model.remote.ShibeService
import com.example.shibeapp.model.response.ShibeDTO
import com.example.shibeapp.view.ShibeFragment
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ShibeRepo @Inject constructor(private val shibeService: ShibeService, @ApplicationContext val context: Context) {

//    private val context = ShibeFragment.ServiceLocator.getContext()
//    private val shibeService by lazy { service.getInstance() }
    val database = ShibeDatabase.getInstance(context)
    val shibeDao = database.shibeDao()
    val favoritesDao = database.favorites()

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cacheShibes: List<Shibe> = shibeDao.getAll()

        if (cacheShibes.size != 0) {
            return@withContext cacheShibes
        } else {
            val shibes: List<Shibe> = shibeService.getShibes().map{ Shibe(url = it) }
            shibeDao.insert(shibes)
            return@withContext shibes
        }
    }

    suspend fun getFavorites() = withContext(Dispatchers.IO) {
        favoritesDao.getFavorites().map {Shibe(it.id, it.url, it.liked)}
    }

    fun heartClick(shibe: Shibe) {
        CoroutineScope(Dispatchers.IO).launch {
            shibeDao.update(shibe)
            if (shibe.liked) favoritesDao.insertFavorite(Favorite(id=shibe.id, url = shibe.url))
            else favoritesDao.delete(Favorite(id=shibe.id, url = shibe.url))
        }
    }
}