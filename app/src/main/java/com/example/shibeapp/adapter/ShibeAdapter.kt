package com.example.shibeapp.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shibeapp.databinding.FragmentShibeBinding
import com.example.shibeapp.databinding.ItemShibeBinding
import com.example.shibeapp.model.ShibeRepo
import com.example.shibeapp.model.local.entity.Shibe
import com.google.android.material.imageview.ShapeableImageView

class ShibeAdapter(
    private val repo: ShibeRepo, private val fragment: FragmentShibeBinding,
    private val favoritesDisplay: Boolean
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    private var shibes = mutableListOf<Shibe>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ShibeViewHolder(
        fragment, repo,
        ItemShibeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        holder.loadShibes(shibes[position], favoritesDisplay)
    }

    override fun getItemCount(): Int {
        return shibes.size
    }

    fun changeList(shibes: List<Shibe>) {
        this.shibes = shibes.toMutableList()
        notifyDataSetChanged()
    }

    class ShibeViewHolder(
        val fragment: FragmentShibeBinding,
        val repo: ShibeRepo,
        val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibes(shibe: Shibe, favoritesDisplay: Boolean) {
            with(binding) {
                laHeart.isVisible = shibe.liked

                with(ivShibe) {
//                    size = if (size > 300) 250 else 500

                    Glide.with(context).load(shibe.url).into(this)
//                    this.load(shibe.url)
//                    Picasso.get().load(shibe.url).into(this)
//                    LoadImage.valueOf("Picasso").loadImage(shibe.url, this)
//                    if(fragment.rvShibes.layoutManager is GridLayoutManager) {
//                        ivShibe.layoutParams.height = Constraints.LayoutParams.MATCH_PARENT
//                        LoadImage.valueOf("loadWithGlide").loadImage(shibe.url, this)
//                    }
//                    else {
//                        ivShibe.layoutParams.height = Constraints.LayoutParams.WRAP_CONTENT
//                        LoadImage.valueOf("loadWithGlide").loadImage(shibe.url, this)
//                    }

                    setOnClickListener {
                        if (!favoritesDisplay) {
                            with(binding.laHeart) {
                                isVisible = !isVisible
                                shibe.liked = isVisible
                                if (isVisible) playAnimation()
                                repo.heartClick(shibe)
                                Log.d("LOGGER", "HEART visible = $isVisible")
                            }
                        }
                    }
                }
            }
        }


        private var ShapeableImageView.size
            get() = height
            set(len) {
                layoutParams.height = len
            }
    }
}