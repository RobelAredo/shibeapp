package com.example.shibeapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.shibeapp.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShibeActivity: AppCompatActivity(R.layout.activity_shibe)