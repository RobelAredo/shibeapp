package com.example.shibeapp.view

import android.graphics.Color
import androidx.annotation.ColorInt
import com.example.shibeapp.model.local.entity.Shibe

data class ShibeState(
    val isLoading: Boolean = false,
    val shibes: List<Shibe> = listOf(),
    val isFavorite: Boolean = false,
    @ColorInt val faveColor: Int = Color.WHITE,
    val layoutManager: RvLayoutManager = RvLayoutManager.GRID,
)

enum class RvLayoutManager {
    LINEAR, GRID, STAGGERED
}