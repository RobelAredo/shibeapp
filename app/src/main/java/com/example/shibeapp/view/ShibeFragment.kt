package com.example.shibeapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shibeapp.R
import com.example.shibeapp.adapter.ShibeAdapter
import com.example.shibeapp.databinding.FragmentShibeBinding
import com.example.shibeapp.model.ShibeRepo
import com.example.shibeapp.viewmodel.ShibeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ShibeFragment @Inject constructor(): Fragment() {
    private var _binding: FragmentShibeBinding? = null
    private val binding get() = _binding!!
    private val shibeViewModel by viewModels<ShibeViewModel>()
    @Inject lateinit var repo: ShibeRepo

//    object ServiceLocator {
//        fun getContext(): Context = this.getContext()
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            sOptions.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {}

                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    shibeViewModel.updateState(layoutManagerPosition = position)
                }
            }

            shibeViewModel.state.observe(viewLifecycleOwner) { state ->

                rvShibes.apply {
                    adapter = ShibeAdapter(repo, fragment = binding, favoritesDisplay = state.isFavorite).apply {
                        changeList(state.shibes)
                    }
                    layoutManager = when (state.layoutManager) {
                        RvLayoutManager.LINEAR -> LinearLayoutManager(context)
                        RvLayoutManager.GRID -> GridLayoutManager(context, 3)
                        RvLayoutManager.STAGGERED -> StaggeredGridLayoutManager(3, 1)
                    }
                }
                favorites.setColorFilter(state.faveColor)
            }


            favorites.setOnClickListener {
                shibeViewModel.updateState(toggleFave = true)
            }
        }

    }
}