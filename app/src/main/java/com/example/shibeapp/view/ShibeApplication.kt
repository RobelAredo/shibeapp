package com.example.shibeapp.view

import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.shibeapp.R
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShibeApplication: Application()